### 概述

Angular8+ 版本中使用 zorro-antd 8.5 版本的案例集合，演示 Angular 以及 zorro 套件的用法

### 案例介绍

* 表格横向滚动条
  左边2列和右边2列锁定后，横向滚动
* 锁定列宽
  表格中使用下拉选择器，当选项内容过长会导致表格列宽自动增大，本案例实现表格列宽锁定，不会跟随内容的增加而自动增大
* 按钮可用性
  zorro 按钮的可用性
* 拖拽行
  * zorro 表格通过拖拽调整行顺序
  * 2020年11月30日 13:14:27 制作服务 dragdrop.service.ts 用于构建表格用到的数据，证明了不管表格数据是在组件的 ts 中还是在对应服务文件中，都不影响拖拽功能
* debounceTime
  防抖，延迟1秒后将上面 input 的内容拷贝到下面
* 数组拷贝
  只拷贝内容，相当于C中传值而不是传址

